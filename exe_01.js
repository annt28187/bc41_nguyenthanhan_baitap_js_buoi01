/* Bài 01. Tính tiền lương nhân viên

- Đầu vào: Số ngày làm việc của nhân viên

- Các bước thực hiện:
   + Tạo biến cho số ngày việc (workingDay) và gán gia trị cho workingDay
   + Tạo hằng số cho lương 1 ngày (ONE_DAY_SALARY)
   + Tạo biến cho tính lương (salary)
   + Sử dụng công thức tính lương = lương 1 ngày * số ngày làm việc 
   =>  salary = ONE_DAY_SALARY * workingDay
   + In kết quả ra console

- Đầu ra: Số tiền lương nhân viên nhận được
*/

const ONE_DAY_SALARY = 100000;
var workingDay = 22;
var salary;

salary = ONE_DAY_SALARY * workingDay;

console.log("Số tiền lương nhân viên nhận được", salary);
