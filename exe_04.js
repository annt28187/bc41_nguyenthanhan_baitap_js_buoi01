/* Bài 04. Tính chu vi và diện tích hình chữ nhật

- Đầu vào: Chiều dài và chiều rộng hình chữ nhật

- Các bước thực hiện:
   + Tạo biến cho chiều dài (long), chiều rộng (width) và gán giá trị cho chiều dài; chiều rộng 
   + Tạo biến cho chu vi (perimeter) và diện tích (area)
   + Sử dụng công thức tính chu vi = (dài + rong)*2 
   =>  perimeter = (long + width) * 2
   + Sử dụng công thức tính diện tích = dài * rộng
   =>  area = long * width
   + In kết quả ra console

- Đầu ra: Chu vi và diện tích hình chữ nhật
*/

var long = 15;
var width = 6;
var perimeter, area;

perimeter = (long + width) * 2;
area = long * width;

console.log("Chu vi hình chữ nhật", perimeter);
console.log("Diện tích hình chữ nhật", area);
