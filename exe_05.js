/* Bài 05. Tính tổng 2 ký số

- Đầu vào: 1 số có 2 chữ số

- Các bước thực hiện:
   + Tạo biến cho số (num) và gán giá trị cho num
   + Tạo biến cho số đơn vị (unit); số hàng chục (ten) và tính tổng (sum)
   + Lấy số hàng đơn vị = so % 10
   =>  unit = num % 10;
   + Lấy số hàng chục = so / 10
   => ten = Math.floor(num / 10);
   + Tổng 2 ký số = số hàng chục * số hàng đơn vị
   =>  sum = ten + unit;
   + In kết quả ra console

- Đầu ra: Tính tổng 2 ký số vừa nhập
*/

var num = 93;
var unit, ten, sum;

unit = num % 10;
ten = Math.floor(num / 10);
sum = ten + unit;

console.log("Tính tổng 2 ký số vừa nhập", sum);
