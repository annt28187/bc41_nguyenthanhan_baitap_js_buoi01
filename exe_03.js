/* Bài 03. Tính tiền quy đổi từ USD sang VND

- Đầu vào: Số USD muốn quy đổi

- Các bước thực hiện:
   + Tạo biến cho số USD (usdMoney)  và gán giá trị cho usdMoney
   + Tạo hằng số cho 1 USD (ONE_DOLLAR)
   + Tạo biến cho số VND (vndMoney)
   + Sử dụng công thức tính quy đổi USD sang VND = 1 USD * số USD 
   =>  vndMoney = ONE_DOLLAR * usdMoney
   + In kết quả ra console

- Đầu ra: Số tiền quy đổi từ USD sang VND
*/

const ONE_DOLLAR = 23500;
var usdMoney = 100;
var vndMoney;

vndMoney = ONE_DOLLAR * usdMoney;

console.log("Số tiền quy đổi từ USD sang VND", vndMoney);
