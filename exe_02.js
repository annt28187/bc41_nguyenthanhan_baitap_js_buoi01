/* Bài 02. Tính giá trị trung bình của 5 số thực

- Đầu vào: 5 số thực

- Các bước thực hiện:
   + Tạo biến cho 5 số thực num1, num2, num3, num4, num5 và gán giá trị cho 5 số thực
   + Tạo biến cho tính trung bình (averageNum)
   + Tính trung bình = Tổng 5 số thực / 5
   =>  averageNum = (num1 +  num2 + num3 + num4 + num5)/5
   + In kết quả ra console

- Đầu ra: Giá trị trung bình của 5 số thực
*/

var num1 = 22;
var num2 = 23;
var num3 = 24;
var num4 = 25;
var num5 = 26;
var averageNum;

averageNum = (num1 + num2 + num3 + num4 + num5) / 5;

console.log("Giá trị trung bình của 5 số thực", averageNum);
